<?php


echo 'Maître Corbeau sur un arbre perché,';
echo 'Tenait en son bec un fromage.';
echo 'Maître Renard par l’odeur alléché';
echo 'Lui tint à peu près ce langage';
echo 'Et bonjour, Monsieur du Corbeau.';
echo 'Que vous êtes joli ! que vous me semblez beau !';
echo 'Sans mentir, si votre ramage';
echo 'Se rapporte à votre plumage';
echo 'Vous êtes le Phenix des hôtes de ces bois.';
echo 'À ces mots le Corbeau ne se sent pas de joie';
echo 'Et pour montrer sa belle voix,';
echo 'Il ouvre un large bec, laisse tomber sa proie.';
echo 'Le Renard s’en saisit, et dit : Mon bon Monsieur,';
echo 'Apprenez que tout flatteur';
echo 'Le Renard s’en saisit, et dit : Mon bon Monsieur,';
echo 'Le Corbeau honteux et confus';
echo 'Apprenez que tout flatteur';
echo 'Vit aux dépens de celui qui l’écoute.';
echo 'Cette leçon vaut bien un fromage sans doute.';
echo 'Le Corbeau honteux et confus';
echo 'Jura, mais un peu tard, qu’on ne l’y prendrait plus.';
echo 'Le Renard s’en saisit, et dit : Mon bon Monsieur,';
echo 'Cette leçon vaut bien un fromage sans doute.';
echo 'Le Corbeau honteux et confus';
echo 'Cette leçon vaut bien un fromage sans doute.';
